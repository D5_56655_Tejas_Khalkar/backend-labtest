
create table movie (
  movie_id integer primary key auto_increment,
  movie_title VARCHAR(200),
  movie_release_date VARCHAR(200),
  movie_time VARCHAR(200),
  director_name VARCHAR(200)
);

insert into movie (movie_title, movie_release_date, movie_time, director_name) values ('3iodits', '26-12-2010', '12.00.00', 'Vindhu Vinod Chopra');
insert into movie (movie_title, movie_release_date, movie_time, director_name) values ('ChakdeIndia', '26-06-2012', '12.30.00', 'sharukh Khan');
insert into movie (movie_title, movie_release_date, movie_time, director_name) values ('Harry Poter', '26-07-2014', '12.00.00', 'vincittet');
insert into movie (movie_title, movie_release_date, movie_time, director_name) values ('Bhaubhali', '26-02-2016', '12.30.00', 'Rajmouli');
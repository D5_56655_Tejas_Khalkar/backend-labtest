const express = require("express");
const db = require("../db");
const utils = require("../utils");
const router = express.Router();

router.post("/add", (request, response) => {
  const {
    movie_title,
    movie_release_date,
    movie_time,
    director_name,
  } = request.body;
  const query = `
          insert into movie
          (movie_title, movie_release_date, movie_time, director_name)
          values
            ('${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')
        `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.delete("/:movie_id", (request, response) => {
  const { movie_id } = request.params;

  const query = `
      delete from movie
      where
      movie_id = ${movie_id}
    `;

  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.put("/update/:movie_id", (request, response) => {
  const { movie_id } = request.params;
  const { movie_title, director_name } = request.body;

  const query = `
      update movie
      set
      movie_title = '${movie_title}',
      director_name = '${director_name}'
      where
      movie_id = ${movie_id}
    `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/get", (request, response) => {
  const query = `
        select
          movie_id,
          movie_title,
          movie_release_date,
          movie_time,
          director_name
        from movie
      `;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});
module.exports = router;
